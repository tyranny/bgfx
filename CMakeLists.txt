set(projname bgfx)
project(${projname})

include_directories(${BX_INCLUDE_DIR})

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    include_directories(${BX_INCLUDE_DIR}/compat/msvc)
    include_directories(3rdparty/dxsdk/include)
elseif(APPLE)
    include_directories(${BX_INCLUDE_DIR}/compat/osx)
endif()

include_directories(3rdparty/khronos)
include_directories(include)

if (APPLE)
    set(SOURCE_FILES src/amalgamated.mm)
else()
    set(SOURCE_FILES src/amalgamated.cpp)
endif()

add_definitions(-DBGFX_CONFIG_MULTITHREADED=0)

if (UNIX)
    add_library(${projname} SHARED ${SOURCE_FILES})
else()
    add_library(${projname} STATIC ${SOURCE_FILES})
endif()

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
    target_link_libraries(${projname} gdi32 psapi)
endif()

if (APPLE)
    target_link_libraries(${projname} "-framework Cocoa")
    target_link_libraries(${projname} "-framework QuartzCore")
    target_link_libraries(${projname} "-weak_framework Metal")
elseif(UNIX)
    find_package(OpenGL REQUIRED)
    target_link_libraries(${projname} ${OPENGL_LIBRARIES})
endif()

set_target_properties(${projname} PROPERTIES LINKER_LANGUAGE CXX)
